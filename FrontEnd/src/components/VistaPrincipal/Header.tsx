import { Row, Col, Image } from "react-bootstrap";
import logo from "../../assets/logo.png";
import { AiOutlineUser } from "react-icons/ai"

function Header() {
  return (
    <Row className="p-2 border-bottom border-dark-subtle d-flex align-items-center">
      <Col className="col-10 d-flex align-items-center">
        <Image fluid width="50" height="50" className="rounded float-start" src={logo}></Image>
      </Col>
      <Col className="col-2">
        <Row className="d-flex align-items-center justify-content-center">
          <Col>
            <AiOutlineUser/>
          </Col>
          <Col className="d-flex align-items-center justify-content-center fw-bold">
            Administrador
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default Header;
