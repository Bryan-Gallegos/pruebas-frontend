import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { VscDebugBreakpointLogUnverified } from "react-icons/vsc";

function ElementoLista(props: any) {
  return (
    <ListGroup.Item className="list-group-item-action ">
      <Link className="text-decoration-none text-reset" to={props.link}>
        <i>
          <VscDebugBreakpointLogUnverified className="me-3" />
        </i>
        <span className="">{props.nombre}</span>
      </Link>
    </ListGroup.Item>
  );
}

export default ElementoLista;
