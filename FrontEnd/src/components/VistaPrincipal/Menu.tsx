import { ListGroup, Col } from "react-bootstrap";
import ElementoLista from "./ElementoLista";

function Menu() {
  return (
    <Col className="col-2 sidebar p-3 vh-100 text-start border-end">
      <ListGroup className="list-group-flush">
        <ElementoLista link="/inicio" nombre="Inicio"/>
        <ElementoLista link="/matricula" nombre="Matrícula" />
        <ElementoLista link="/contrato" nombre="Contrato" />
        <ElementoLista link="/pagos" nombre="Pagos" />
        <ElementoLista link="/codigosycorreo" nombre="Código y correos" />
        <ElementoLista link="/observaciones" nombre="Observaciones" />
      </ListGroup>
    </Col>
  );
}

export default Menu;
