import './App.css'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import Inicio from './pages/Inicio/Inicio'
import Ingreso from './pages/Acceso/Ingreso'
import Matricula from './pages/Matricula/Matricula'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/ingreso" element={<Ingreso />} />
        <Route path="/" element={<Navigate to="/ingreso"/>} />
        <Route path='/inicio' element={<Inicio/>} />
        <Route path='/matricula' element={<Matricula/>} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
