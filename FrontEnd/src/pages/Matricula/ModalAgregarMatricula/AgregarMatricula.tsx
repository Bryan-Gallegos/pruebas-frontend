import React, { useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './AgregarMatricula.css'; // Importa el archivo de estilos personalizados
import {Modal, Button, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label} from 'reactstrap';


interface ModalAgregarMatriculaProps {
    isOpen: boolean;
    onRequestClose: () => void;
}

const ModalAgregarMatricula: React.FC<ModalAgregarMatriculaProps> = ({ isOpen, onRequestClose }) => {
    const [activeTab, setActiveTab] = useState<number>(0);
   
    const handleTabSelect = (index: number) => {
        setActiveTab(index);
      };

  return (
    <Modal isOpen={isOpen} onRequestClose={onRequestClose}  size="lg" centered> 
      <ModalHeader onRequestClose={onRequestClose} className='modal-header-custom'>
        <div className="d-flex justify-content-between align-items-center">
            <h6 className="mb-0">AGREGAR A LA MATRÍCULA</h6>
        </div>
      </ModalHeader>

      <ModalBody>
        <Tabs selectedIndex={activeTab} onSelect={handleTabSelect}>
            <TabList className="custom-tab-list">
                <Tab className={`custom-tab ${activeTab === 0 ? 'custom-tab--selected' : ''}`}>
                    Archivos del Estudiante
                </Tab>
                <Tab className={`custom-tab ${activeTab === 1 ? 'custom-tab--selected' : ''}`}>
                    Datos del Padre
                </Tab>
                <Tab className={`custom-tab ${activeTab === 2 ? 'custom-tab--selected' : ''}`}>
                    Datos de la Madre
                </Tab>
                <Tab className={`custom-tab ${activeTab === 3 ? 'custom-tab--selected' : ''}`}>
                    Datos del Apoderado
                </Tab>
            </TabList>

          <TabPanel>
            <FormGroup className="labelRow">
                <div className="label-container">
                    <Label for="nombreLabel" className="label">NOMBRE: </Label>
                    <Label for="nombreLabel1" className="label">JOSE MIGUEL VERA MAMANI</Label>
                </div>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Contrato Firmado</span>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Constancia de Vacante</span>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Declaración Jurada</span>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Ficha única de Matricula</span>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Certificado de Estudios</span>
            </FormGroup>

          </TabPanel>

          {/*--------------------------------PANEL 2--------------------------------------------------*/}

          <TabPanel>
            <FormGroup className="labelRow">
                <div className="label-container">
                    <Label for="nombreLabel" className="label">NOMBRE: </Label>
                    <Label for="nombreLabel1" className="label">JOSE MIGUEL VERA MAMANI</Label>
                </div>
            </FormGroup>

            <FormGroup className="labelRow">
                <div className="label-with-margin">
                    <span> DATOS DEL PADRE </span>
                </div>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="nombres">NOMBRES</Label>
              <Input type="text" id="nombre" placeholder='INGRESE NOMBRES DEL PADRE'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="apellidos">APELLIDOS</Label>
              <Input type="text" id="apellidos" placeholder='INGRESE APELLIDOS DEL PADRE'/>
            </FormGroup>

            <FormGroup className='inputRow'>
                <Label for="fechaNacimiento">FECHA DE NACIMIENTO</Label>
                <Input type="date" id="dateInput"/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="dni">DNI</Label>
              <Input type="text" id="dni" placeholder='INGRESE N° DNI'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="celular">CELULAR</Label>
              <Input type="text" id="celular" placeholder='INGRESE N° CELULAR'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="direccion">DIRECCIÓN</Label>
              <Input type="text" id="direccion" placeholder='INGRESE DIRECCION'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="distrito">DISTRITO</Label>
              <Input type="text" id="distrito" placeholder='INGRESE DISTRITO'/>
            </FormGroup>

            <div className="inputRow">
              <Label for="nivelEstudios">NIVEL DE ESTUDIOS</Label>
              <Input type="select" id="nivelEstudios">
                <option value="nivelEstudios1">Basico</option>
                <option value="nivelEstudios2">Tecnico Profeisonal</option>
                <option value="nivelEstudios3">Universitario</option>
              </Input>
            </div>

            <FormGroup className="inputRow">
              <Label for="trabajo">CENTRO DE TRABAJO</Label>
              <Input type="text" id="trabajo" placeholder='INGRESE CENTRO DE TRABAJO'/>
            </FormGroup>
          </TabPanel>

          {/*--------------------------------PANEL 3--------------------------------------------------*/}


          <TabPanel>           
            <FormGroup className="labelRow">
                <div className="label-container">
                    <Label for="nombreLabel" className="label">NOMBRE: </Label>
                    <Label for="nombreLabel1" className="label">JOSE MIGUEL VERA MAMANI</Label>
                </div>
            </FormGroup>

            <FormGroup className="labelRow">
                <div className="label-with-margin">
                    <span> DATOS DE LA MADRE </span>
                </div>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="nombresMadre">NOMBRES</Label>
              <Input type="text" id="nombresMadre" placeholder='INGRESE NOMBRES DE LA MADRE'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="apellidosMadre">APELLIDOS</Label>
              <Input type="text" id="apellidosMadre" placeholder='INGRESE APELLIDOS DE LA MADRE'/>
            </FormGroup>

            <FormGroup className='inputRow'>
                <Label for="fechaNacimientoMadre">FECHA DE NACIMIENTO</Label>
                <Input type="date" id="dateInput"/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="dniMadre">DNI</Label>
              <Input type="text" id="dniMadre" placeholder='INGRESE N° DNI'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="celularMadre">CELULAR</Label>
              <Input type="text" id="celularMadre" placeholder='INGRESE N° CELULAR'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="direccionMadre">DIRECCIÓN</Label>
              <Input type="text" id="direccionMadre" placeholder='INGRESE DIRECCION'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="distritoMadre">DISTRITO</Label>
              <Input type="text" id="distritoMadre" placeholder='INGRESE DISTRITO'/>
            </FormGroup>

            <div className="inputRow">
              <Label for="nivelEstudiosMadre">NIVEL DE ESTUDIOS</Label>
              <Input type="select" id="nivelEstudiosMadre">
                <option value="nivelEstudiosMadre1">Basico</option>
                <option value="nivelEstudiosMadre2">Tecnico Profeisonal</option>
                <option value="nivelEstudiosMadre3">Universitario</option>
              </Input>
            </div>

            <FormGroup className="inputRow">
              <Label for="trabajoMadre">CENTRO DE TRABAJO</Label>
              <Input type="text" id="trabajoMadre" placeholder='INGRESE CENTRO DE TRABAJO'/>
            </FormGroup>
          </TabPanel>


          {/*--------------------------------PANEL 4--------------------------------------------------*/}


          <TabPanel>
          <FormGroup className="labelRow">
                <div className="label-container">
                    <Label for="nombreLabel" className="label">NOMBRE: </Label>
                    <Label for="nombreLabel1" className="label">JOSE MIGUEL VERA MAMANI</Label>
                </div>
            </FormGroup>

            <FormGroup className="labelRow">
                <div className="label-with-margin">
                    <span> DATOS DEL APODERADO</span>
                </div>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="nombresApoderado">NOMBRES</Label>
              <Input type="text" id="nombresApoderado" placeholder='INGRESE NOMBRES DEL APODERADO'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="apellidosApoderado">APELLIDOS</Label>
              <Input type="text" id="apellidosApoderado" placeholder='INGRESE APELLIDOS DEl APODERADO'/>
            </FormGroup>

            <FormGroup className='inputRow'>
                <Label for="fechaNacimientoApoderado">FECHA DE NACIMIENTO</Label>
                <Input type="date" id="dateInput"/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="dniApoderado">DNI</Label>
              <Input type="text" id="dniApoderado" placeholder='INGRESE N° DNI'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="celularApoderado">CELULAR</Label>
              <Input type="text" id="celularApoderado" placeholder='INGRESE N° CELULAR'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="direccionApoderado">DIRECCIÓN</Label>
              <Input type="text" id="direccionApoderado" placeholder='INGRESE DIRECCION'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="distritoApoderado">DISTRITO</Label>
              <Input type="text" id="distritoApoderado" placeholder='INGRESE DISTRITO'/>
            </FormGroup>

            <div className="inputRow">
              <Label for="nivelEstudiosApoderado">NIVEL DE ESTUDIOS</Label>
              <Input type="select" id="nivelEstudiosApoderado">
                <option value="nivelEstudiosApoderado1">Basico</option>
                <option value="nivelEstudiosApoderado2">Tecnico Profeisonal</option>
                <option value="nivelEstudiosApoderado3">Universitario</option>
              </Input>
            </div>

            <FormGroup className="inputRow">
              <Label for="trabajoApoderado">CENTRO DE TRABAJO</Label>
              <Input type="text" id="trabajoApoderado" placeholder='INGRESE CENTRO DE TRABAJO'/>
            </FormGroup>
          </TabPanel>


        </Tabs>
      </ModalBody>
      <ModalFooter className="d-flex justify-content-end">
        <FormGroup className="BotonGuardar">
          <Button>
            GUARDAR
          </Button>
        </FormGroup>
        <FormGroup className="BotonCancelar">
          <Button onClick={onRequestClose}>
            CANCELAR
          </Button>
        </FormGroup>
      </ModalFooter>
    </Modal>
  );
};

export default ModalAgregarMatricula;