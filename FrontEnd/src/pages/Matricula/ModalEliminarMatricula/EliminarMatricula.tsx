import React, { useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './EliminarMatricula.css'; // Importa el archivo de estilos personalizados
import {Modal, Button, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label} from 'reactstrap';


interface ModalEliminarMatriculaProps {
    isOpen: boolean;
    onRequestClose: () => void;
}

const ModalEliminarMatricula: React.FC<ModalEliminarMatriculaProps> = ({ isOpen, onRequestClose }) => {
   

  return (
    <Modal isOpen={isOpen} onRequestClose={onRequestClose}   centered> 

      <ModalBody>
        <span>
            ¿Seguro que quieres eliminar la matricula de JOSE MIGUEL VERA MAMANI?
        </span>
      </ModalBody>
      <ModalFooter className="d-flex justify-content-end">
        <FormGroup>
          <Button onClick={onRequestClose}>
            CANCELAR
          </Button>
        </FormGroup>
        <FormGroup className="BotonCancelar">
          <Button>
            ELIMINAR MATRICULA
          </Button>
        </FormGroup>
      </ModalFooter>
    </Modal>
  );
};

export default ModalEliminarMatricula;