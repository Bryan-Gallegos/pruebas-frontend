import React, { useState } from 'react';
import { Container, Row, Col } from "react-bootstrap";
import Menu from "../../components/VistaPrincipal/Menu";
import Header from "../../components/VistaPrincipal/Header";
import {FormGroup, Input, Label, Button, Table} from 'reactstrap';
import { FaEye, FaPlus, FaEdit, FaTrash } from "react-icons/fa";
import './Matricula.css'; // Importa el archivo de estilos personalizados
import ModalNuevaMatricula from './ModalNuevaMatricula/NuevaMatricula';
import ModalVerMatricula from './ModalVerMatricula/VerMatricula';
import ModalAgregarMatricula from './ModalAgregarMatricula/AgregarMatricula';
import ModalEditarMatricula from './ModalEditarMatricula/EditarMatricula';
import ModalEliminarMatricula from './ModalEliminarMatricula/EliminarMatricula';

const Matricula: React.FC = () => {

  /*PARA ABRIR Y CERRAR MODAL DE NUEVA MATRICULA*/
  const [modalAbierto, setModalAbierto] = useState(false);

  const ManejarModalAbierto = () => {
    setModalAbierto(true);
    setModalAbiertoVer(false);
    setModalAbiertoAgregar(false);
    setModalAbiertoEditar(false);
    setModalAbiertoEliminar(false);
  };

  const ManejarModalCerrado = () => {
    setModalAbierto(false);
  };

  /*PARA ABRIR Y CERRAR MODAL DE VER MATRICULA*/
  const [modalAbiertoVer, setModalAbiertoVer] = useState(false);

  const ManejarModalAbiertoVer = () => {
    setModalAbierto(false);
    setModalAbiertoVer(true);
    setModalAbiertoAgregar(false);
    setModalAbiertoEditar(false);
    setModalAbiertoEliminar(false);
  };

  const ManejarModalCerradoVer = () => {
    setModalAbiertoVer(false);
  };

  /*PARA ABRIR Y CERRAR MODAL DE AGREGAR MATRICULA*/
  const [modalAbiertoAgregar, setModalAbiertoAgregar] = useState(false);

  const ManejarModalAbiertoAgregar = () => {
    setModalAbierto(false);
    setModalAbiertoVer(false);
    setModalAbiertoAgregar(true);
    setModalAbiertoEditar(false);
    setModalAbiertoEliminar(false);
  };

  const ManejarModalCerradoAgregar = () => {
    setModalAbiertoAgregar(false);
  };

  /*PARA ABRIR Y CERRAR MODAL DE EDITAR MATRICULA*/
  const [modalAbiertoEditar, setModalAbiertoEditar] = useState(false);

  const ManejarModalAbiertoEditar = () => {
    setModalAbierto(false);
    setModalAbiertoVer(false);
    setModalAbiertoAgregar(false);
    setModalAbiertoEditar(true);
    setModalAbiertoEliminar(false);
  };

  const ManejarModalCerradoEditar = () => {
    setModalAbiertoEditar(false);
  };

  /*PARA ABRIR Y CERRAR MODAL DE ELIMINAR MATRICULA*/
  const [modalAbiertoEliminar, setModalAbiertoEliminar] = useState(false);

  const ManejarModalAbiertoEliminar = () => {
    setModalAbierto(false);
    setModalAbiertoVer(false);
    setModalAbiertoAgregar(false);
    setModalAbiertoEditar(false);
    setModalAbiertoEliminar(true);
  };

  const ManejarModalCerradoEliminar = () => {
    setModalAbiertoEliminar(false);
  };




    return (
        <Container fluid className="">
        <Header />
        <Row className="">
          <Menu />
          <Col className="col-10 ">
            <div className="matriculaTitulo">
                    MATRICULA
            </div>
            <FormGroup className="Formulario">
              <FormGroup className="inputRow">
                <Label for="nombre">NOMBRE</Label>
                <Input type="text" id="nombre"placeholder="DIGITE NOMBRE DEL ALUMNO"/>
              </FormGroup>
          

              <FormGroup className="formContainer">
                <div className="form-row">
                  <div className="form-column0">
                    <Label for="dni">DNI</Label>
                    <Input type="text" id="dni"placeholder="DIGITE N° DNI"/>
                  </div>

                  <div className="form-column1">
                    <Label for="nivel">NIVEL</Label>
                    <Input type="select" id="nivel">
                      <option value="nivel1">Inicial</option>
                      <option value="nivel2">Primaria</option>
                      <option value="nivel3">Secundaria</option>
                    </Input>
                  </div>

                  <div className="form-column1">
                    <Label for="grado">GRADO</Label>
                    <Input type="select" id="grado">
                      <option value="grado1">Primero</option>
                      <option value="grado2">Segundo</option>
                      <option value="grado3">Tercero</option>
                      <option value="grado4">Cuarto</option>
                      <option value="grado5">Quinto</option>
                      <option value="grado6">Sexto</option>
                    </Input>
                  </div>
                </div>
              </FormGroup>

              <FormGroup className="BotonBuscar">
                <Button>
                  BUSCAR
                </Button>
              </FormGroup>
            </FormGroup>

            <FormGroup className="Tabla">

              <FormGroup className="BotonNuevaMatricula">
                  <Button onClick={ManejarModalAbierto}>
                    NUEVA MATRICULA
                  </Button>
              </FormGroup>

              <ModalNuevaMatricula isOpen={modalAbierto} onRequestClose={ManejarModalCerrado} />

              <Table>
                <thead>
                  <tr>
                    <th>DNI</th>
                    <th>NOMBRES Y APELLIDOS</th>
                    <th>NIVEL</th>
                    <th>GRADO</th>
                    <th>ACCIONES</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>72451239</td>
                    <td>Jose Miguel Vera Mamani</td>
                    <td>Primaria</td>
                    <td>Sexto</td>
                    <td>
                      <FormGroup className="Boton">
                        <Button  onClick={ManejarModalAbiertoVer}><FaEye />
                          <ModalVerMatricula isOpen={modalAbiertoVer} onRequestClose={ManejarModalCerradoVer} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoAgregar}><FaPlus />
                          <ModalAgregarMatricula isOpen={modalAbiertoAgregar} onRequestClose={ManejarModalCerradoAgregar} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoEditar}><FaEdit />
                          <ModalEditarMatricula isOpen={modalAbiertoEditar} onRequestClose={ManejarModalCerradoEditar} />
                        </Button>{' '}
                        <Button style={{ background: '#e02a25', color: 'white' }} onClick={ManejarModalAbiertoEliminar}><FaTrash />
                          <ModalEliminarMatricula isOpen={modalAbiertoEliminar} onRequestClose={ManejarModalCerradoEliminar} />
                        </Button>
                      </FormGroup>
                    </td>
                  </tr>
                  <tr>
                    <td>72452365</td>
                    <td>Jimy Gabriel Revilla Tellez</td>
                    <td>Secundaria</td>
                    <td>Tercero</td>
                    <td>
                      <FormGroup className="Boton">
                        <Button  onClick={ManejarModalAbiertoVer}><FaEye />
                          <ModalVerMatricula isOpen={modalAbiertoVer} onRequestClose={ManejarModalCerradoVer} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoAgregar}><FaPlus />
                          <ModalAgregarMatricula isOpen={modalAbiertoAgregar} onRequestClose={ManejarModalCerradoAgregar} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoEditar}><FaEdit />
                          <ModalEditarMatricula isOpen={modalAbiertoEditar} onRequestClose={ManejarModalCerradoEditar} />
                        </Button>{' '}
                        <Button style={{ background: '#e02a25', color: 'white' }} onClick={ManejarModalAbiertoEliminar}><FaTrash />
                          <ModalEliminarMatricula isOpen={modalAbiertoEliminar} onRequestClose={ManejarModalCerradoEliminar} />
                        </Button>
                      </FormGroup>
                    </td>
                  </tr>
                  <tr>
                    <td>71203214</td>
                    <td>Carlos Bryan Gallegos Batallanos</td>
                    <td>Secundaria</td>
                    <td>Cuarto</td>
                    <td>
                      <FormGroup className="Boton">
                        <Button  onClick={ManejarModalAbiertoVer}><FaEye />
                          <ModalVerMatricula isOpen={modalAbiertoVer} onRequestClose={ManejarModalCerradoVer} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoAgregar}><FaPlus />
                          <ModalAgregarMatricula isOpen={modalAbiertoAgregar} onRequestClose={ManejarModalCerradoAgregar} />
                        </Button>{' '}
                        <Button  onClick={ManejarModalAbiertoEditar}><FaEdit />
                          <ModalEditarMatricula isOpen={modalAbiertoEditar} onRequestClose={ManejarModalCerradoEditar} />
                        </Button>{' '}
                        <Button style={{ background: '#e02a25', color: 'white' }} onClick={ManejarModalAbiertoEliminar}><FaTrash />
                          <ModalEliminarMatricula isOpen={modalAbiertoEliminar} onRequestClose={ManejarModalCerradoEliminar} />
                        </Button>
                      </FormGroup>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </FormGroup>
          </Col>
        </Row>
      </Container>
    );
  }
export default Matricula;