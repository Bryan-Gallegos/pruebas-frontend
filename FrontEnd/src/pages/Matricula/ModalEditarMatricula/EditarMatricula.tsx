import React, { useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './EditarMatricula.css'; // Importa el archivo de estilos personalizados
import { FaSearch } from "react-icons/fa";
import {Modal, Button, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label} from 'reactstrap';


interface ModalEditarMatriculaProps {
    isOpen: boolean;
    onRequestClose: () => void;
}

const ModalEditarMatricula: React.FC<ModalEditarMatriculaProps> = ({ isOpen, onRequestClose }) => {
    const [activeTab, setActiveTab] = useState<number>(0);
   
    const handleTabSelect = (index: number) => {
        setActiveTab(index);
      };

  return (
    <Modal isOpen={isOpen} onRequestClose={onRequestClose}  size="lg" centered> 
      <ModalHeader onRequestClose={onRequestClose} className='modal-header-custom'>
        <div className="d-flex justify-content-between align-items-center">
            <h6 className="mb-0">MATRÍCULA</h6>
        </div>
      </ModalHeader>

      <ModalBody>
        <Tabs selectedIndex={activeTab} onSelect={handleTabSelect}>
          <TabList className="custom-tab-list">
            <Tab className={`custom-tab ${activeTab === 0 ? 'custom-tab--selected' : ''}`}>
              Información Estudiante
            </Tab>
            <Tab className={`custom-tab ${activeTab === 1 ? 'custom-tab--selected' : ''}`}>
              Información Contratante
            </Tab>
            <Tab className={`custom-tab ${activeTab === 2 ? 'custom-tab--selected' : ''}`}>
              Pago Matrícula
            </Tab>
            <Tab className={`custom-tab ${activeTab === 3 ? 'custom-tab--selected' : ''}`}>
              Pago Guía
            </Tab>
          </TabList>

          <TabPanel className='tabpanel'>
            <FormGroup className="inputRow1">
              <Label for="dni">N° DNI</Label>
              <Input type="text" id="dni" placeholder='INGRESE DNI'/>
              <FaSearch size={32}/>
            </FormGroup>
            <FormGroup className="inputRow">
              <Label for="nombre">NOMBRES</Label>
              <Input type="text" id="nombre" placeholder='INGRESE NOMBRES'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="apellido">APELLIDOS</Label>
              <Input type="text" id="apellido"placeholder='INGRESE APELLIDOS'/>
            </FormGroup>

            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label for="nivel">NIVEL</Label>
                  <Input type="select" id="nivel">
                    <option value="nivel1">Inicial</option>
                    <option value="nivel2">Primaria</option>
                    <option value="nivel3">Secundaria</option>
                  </Input>
                </div>

                <div className="form-column">
                  <Label for="grado">GRADO</Label>
                  <Input type="select" id="grado">
                    <option value="grado1">Primero</option>
                    <option value="grado2">Segundo</option>
                    <option value="grado3">Tercero</option>
                    <option value="grado4">Cuarto</option>
                    <option value="grado5">Quinto</option>
                    <option value="grado6">Sexto</option>
                  </Input>
                </div>

                <div className="form-column">
                  <Label for="dateInput">FECHA DE NACIMIENTO</Label>
                  <Input type="date" id="dateInput"/>
                </div>

              </div>
            </FormGroup>

            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label for="colegio">COLEGIO</Label>
                  <Input type="text" id="colegio" placeholder='COLEGIO DE PROCEDENCIA'/>
                </div>

                <div className="form-column">
                  <Label for="destreza">DESTREZA</Label>
                  <Input type="text" id="destreza" placeholder='DESTREZA'/>
                </div>

              </div>
            </FormGroup>

          </TabPanel>

          {/*--------------------------------PANEL 2--------------------------------------------------*/}

          <TabPanel>
            <FormGroup className="inputRow">
              <Label for="nombre">CONTRATANTE</Label>
              <Input type="text" id="nombre" placeholder='INGRESE NOMBRES DEL CONTRATANTE'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="DNI">N° DNI</Label>
              <Input type="text" id="nombre" placeholder='INGRESE DNI'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="nombre">DIRECCION</Label>
              <Input type="text" id="nombre" placeholder='INGRESE LA DIRECCION'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Label for="nombre">PARENTESCO</Label>
              <Input type="text" id="nombre" placeholder='CUAL ES EL PARENTESCO'/>
            </FormGroup>

            <FormGroup className="inputRow">
              <Input type="checkbox"/>
              <span className='labelCheckbox'>Firmó Contrato</span>
            </FormGroup>
          </TabPanel>

          {/*--------------------------------PANEL 3--------------------------------------------------*/}


          <TabPanel>
            <div className="inputRow">
              <Label for="condicion">CONDICION</Label>
              <Input type="select" id="condicion">
                <option value="condicion1">Nuevo</option>
                <option value="condicion2">Antiguo</option>
              </Input>
            </div>

            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label className="label-with-margin1" for="añoPasado">MATRÍCULA AÑO ANTERIOR</Label>
                  <Label className="label-with-margin" for="añoPasado1">S/. 230.00</Label>
                </div>

                <div className="form-column">
                  <Label className="label-with-margin1" for="añoActual">MATRÍCULA AÑO ACTUAL</Label>
                  <Label className="label-with-margin" for="añoActual1">S/. 330.00</Label>
                </div>
              </div>
            </FormGroup>

            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label className="label-with-margin1"for="descuento">DESCUENTO</Label>
                  <Input type="number" id="dateInput"/>

                </div>

                <div className="form-column">
                  <Label className="label-with-margin1" for="matriculaPactada">MATRÍCULA PACTADA</Label>
                  <Label className="label-with-margin" for="matriculaPactada1">S/. 290.00</Label>
                </div>
              </div>
            </FormGroup>

            <div className="inputRow">
              <Label className="label-with-margin1"  for="resposablePacto">RESPONSABLE DEL PACTO</Label>
              <Input className="label-with-margin1" type="select" id="responsablePacto">
                <option value="responsablePacto1">Miguel Armas</option>
                <option value="resposablePacto2">Yanira Rivera</option>
                <option value="resposablePacto2">Marco Vilca</option>
              </Input>
            </div>
          </TabPanel>


          {/*--------------------------------PANEL 4--------------------------------------------------*/}


          <TabPanel>
            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label className="label-with-margin1" for="guiasAñoPasado">GUÍAS AÑO ANTERIOR</Label>
                  <Label className="label-with-margin" for="guiasAñoPasado1">S/. 90.00</Label>
                </div>

                <div className="form-column">
                  <Label className="label-with-margin1" for="guiasAñoActual">GUÍAS AÑO ACTUAL</Label>
                  <Label className="label-with-margin" for="guiasAñoActual1">S/. 140.00</Label>
                </div>
              </div>
            </FormGroup>

            <FormGroup className="formContainer">
              <div className="form-row">
                <div className="form-column">
                  <Label className="label-with-margin1"for="descuento1">DESCUENTO</Label>
                  <Input type="number" id="dateInput"/>

                </div>

                <div className="form-column">
                  <Label className="label-with-margin1" for="guiasPactada">MONTO PACTADO</Label>
                  <Label className="label-with-margin" for="guiasPactada1">S/. 140.00</Label>
                </div>
              </div>
            </FormGroup>

            <div className="inputRow">
              <Label className="label-with-margin1"  for="resposablePacto">RESPONSABLE DEL PACTO</Label>
              <Input className="label-with-margin1" type="select" id="responsablePacto">
                <option value="responsablePacto1">Miguel Armas</option>
                <option value="resposablePacto2">Yanira Rivera</option>
                <option value="resposablePacto2">Marco Vilca</option>
              </Input>
            </div>
          </TabPanel>


        </Tabs>
      </ModalBody>
      <ModalFooter className="d-flex justify-content-end">
        <FormGroup className="BotonGuardar">
          <Button>
            GUARDAR
          </Button>
        </FormGroup>
        <FormGroup className="BotonCancelar">
          <Button onClick={onRequestClose}>
            CANCELAR
          </Button>
        </FormGroup>
      </ModalFooter>
    </Modal>
  );
};

export default ModalEditarMatricula;