import "./Ingreso.css";
import { useForm } from "react-hook-form";
import logoReich from "../../assets/logo.png";
import { Form, Button, InputGroup } from "react-bootstrap";
import Image from "react-bootstrap/Image";
import Card from "react-bootstrap/Card";
import { Container, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

function Ingreso() {

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>();

  type FormValues = {
    usuario: String;
    contraseña: String;
  };
  const onSubmit = (data: FormValues) => {
    console.log(data);
    navigate("/inicio");
  };

  return (
    <Container fluid className="vh-100">
      <Row className="vh-100">
        <Col className="logoCol" xs={12} md={6}>
          <Image src={logoReich} className="logo" alt="Reich Logo" fluid />
        </Col>
        <Col className="formCol" xs={12} md={6}>
          <Card style={{ width: "35rem" }}>
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Row className="m-5">
                <InputGroup className="">
                  <Col className="">
                    <InputGroup.Text
                      id="icon_usuario"
                      className="justify-content-center"
                    >
                      @
                    </InputGroup.Text>
                  </Col>
                  <Col xs={10} md={10} lg={10} xl={10}>
                    <Form.Group>
                      <Form.Control
                        type="text"
                        id="usuario"
                        placeholder="Usuario"
                        autoComplete="usuario"
                        aria-describedby="icon_usuario"
                        {...register("usuario", { required: true })}
                      />
                    </Form.Group>
                  </Col>
                </InputGroup>
                {errors.usuario && (
                  <Form.Text as={Col} style={{ color: "crimson" }}>
                    Por favor, ingrese un usuario
                  </Form.Text>
              )}
              </Row>

              <Row className="m-5">
                <InputGroup>
                  <Col className="">
                    <InputGroup.Text
                      id="icon_usuario"
                      className="justify-content-center"
                    >
                      @
                    </InputGroup.Text>
                  </Col>
                  <Col xs={10} md={10} lg={10} xl={10}>
                    <Form.Group>
                      <Form.Control
                        className="form-control"
                        type="password"
                        id="contraseña"
                        placeholder="Contraseña"
                        autoComplete="current-password"
                        {...register("contraseña", { required: true })}
                      />
                    </Form.Group>
                  </Col>
                </InputGroup>
                {errors.contraseña && (
                  <Form.Text style={{ color: "crimson" }}>
                    Por favor, ingrese una contraseña
                  </Form.Text>
                )}
              </Row>

              <Row className="">
                <div className="d-grid gap-2">
                  <Button type="submit" variant="danger" className="">
                    Ingresar
                  </Button>
                </div>
              </Row>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Ingreso;
