import { Container, Row, Col } from "react-bootstrap";
import Menu from "../../components/VistaPrincipal/Menu";
import Header from "../../components/VistaPrincipal/Header";

function Inicio() {
  return (
    <Container fluid className="">
      <Header />
      <Row className="">
        <Menu />
        <Col className="col-10">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere
          non enim eget scelerisque. Aenean consequat erat sit amet suscipit
          vulputate. Pellentesque consectetur at nunc vel dignissim. Morbi a
          dignissim sapien. Aenean lobortis id quam ut faucibus. Nullam sed nisl
          at lectus convallis maximus. Cras nunc diam, lacinia non dapibus id,
          tincidunt in massa. Vivamus non tortor purus. Suspendisse potenti.
          Maecenas semper risus urna. Nullam id ante pretium, sagittis sapien
          nec, viverra odio. Proin lacinia nunc velit, eu semper augue
          consectetur vitae. In at sollicitudin lorem.
        </Col>
      </Row>
    </Container>
  );
}

export default Inicio;
